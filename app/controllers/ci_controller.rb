require 'yaml'

class CiController < ApplicationController
  def index
    render json: YAML.load_file((Rails.root.join('.gitlab-ci.yml')))
  end
end
