source 'https://rubygems.org'

gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1'
gem 'rake'
gem 'sqlite3'

group :test do
  gem 'minitest'
end

group :development do
  gem 'brakeman'
  gem 'bundler-audit',
      git: 'https://github.com/kmcphillips/bundler-audit.git',
      ref: 'c38478e316ec7c6c7876fb58110bacc3e6977899'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop'
end
