require 'minitest/autorun'

class TestDummy < Minitest::Test
  def test_dummy_addition
    assert_equal 2, 1 + 1
  end

  def test_dummy_substraction
    assert_equal 0, 1 - 1
  end

  def test_dummy_raise
    assert_raises ZeroDivisionError do
      1 / 0
    end
  end
end
